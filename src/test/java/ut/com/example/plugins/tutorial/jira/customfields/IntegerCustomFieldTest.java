package ut.com.example.plugins.tutorial.jira.customfields;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import com.example.plugins.tutorial.jira.customfields.IntegerCustomField;

import static org.mockito.Mockito.*;

/**
 * @since 3.5
 */
public class IntegerCustomFieldTest {

    @Before
    public void setup() {

    }

    @After
    public void tearDown() {

    }

    @Test(expected=Exception.class)
    public void testSomething() throws Exception {

        //IntegerCustomField testClass = new IntegerCustomField();

        throw new Exception("IntegerCustomField has no tests!");

    }

}
