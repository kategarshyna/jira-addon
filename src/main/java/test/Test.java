package test;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.project.Project;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

import javax.servlet.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


public class Test extends HttpServlet{
    private static final Logger log = LoggerFactory.getLogger(Test.class);

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException
    {
        String content = "<h2>All Existing Projects : </h2> ";
        String currentUser = request.getRemoteUser();

        response.setContentType("text/html");

        if(currentUser == null) {
            response.getWriter().write("<html><body> Please, Sign In first! </body></html>");
        } else {
            List<Project> projects = getAllProjects();
            if (projects.size() > 0) {
                for (Project project : projects) {
                    content = content + "<b>" + project.getName() + "</b> - " + project.getKey() + " </br> ";
                }
            } else {
                content = content + "There are no Projects at the moment!";
            }


        }

        response.getWriter().write("<html><head><style>table, th, td {border: 1px solid black;}</style>"
                + "</head><body>Current User : "
                + currentUser
                + "</br>"
                + content
                + "</table></body></html>");

    }


    private List<Project> getAllProjects () {
        return ComponentAccessor.getProjectManager().getProjects();
    }

}
